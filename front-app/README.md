1. 梳理业务需求
- 整体流程
- 公共方法或者公共组件
- 选择第三方sdk

2. 基础建设
- 团队协作
  - 代码管理 gitlab
  
  - 代码风格 
    语法层面 eslint 统一约束
    风格层面 prettier 统一风格
    代码提交 husky git钩子函数

    husky 使用流程
    1. 下载`husky`
    2. 执行`husky install`， 生成.husky文件夹
    3. 执行`npx husky add .husky/pre-commit "npm run lint"` , 添加`pre-commit`的hook在提交之前执行npm run lint

    4. 执行`npx husky add .husky/commit-msg "npm run commitmsg"` , 添加`commit-msg`的hook在提交时执行commitlint校验提交信息
      `"commitmsg": "commitlint -e $GIT_PARAMS"`: 找`commitlint`命令行工具去执行所以要安装`@commitlint/cli`,才能执行`commitlint`指令, 创建`commitlint.config.js`文件，配置提交信息语法。直接用了`@commitlint/config-conventional`现成的语法包

- 目录规范，样式约定
  - 样式层面
    考虑项目整体适配，单位 px、rem（html fontsize）、em（父元素的fontsize）
    css预编译语言： less sass stylus， 考虑配套ui组件


3. 项目搭建
基于[`vue/cli`](https://cli.vuejs.org/zh/guide/)来进行二次搭建


4. 区分不同环境
根据 `process.env` 环境变量来区分环境的

- 开发 `development`
- 生产 `production`
- 测试 `test`