import zh from "./config/zh"
import en from "./config/en"
import { createI18n } from "vue-i18n"

const messages = {
  zh,
  en,
}

const i18n = createI18n({
  locale: "zh", // set locale
  fallbackLocale: "en", // set fallback locale
  legacy: false,
  messages,
})

export default i18n
