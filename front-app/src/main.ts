import { createApp } from "vue"
import { createPinia } from "pinia"
import App from "./App.vue"
import router from "./router"
import uiPlugin from "@/plugin/ui.registry"
import baseComponent from "@/plugin/baseComponet.registry"
import "@/styles/theme.css"
import "@/styles/common.less"
import "ant-design-vue/dist/antd.css"
import i18n from "@/i18n/index"
createApp(App)
  .use(createPinia())
  .use(i18n)
  .use(router)
  .use(uiPlugin)
  .use(baseComponent)
  .mount("#app")
