import { inject, watch, ref } from "vue"
import dayjs from "dayjs"
import relativeTime from "dayjs/plugin/relativeTime"
import "dayjs/locale/zh-cn.js"
import "dayjs/locale/en.js"
dayjs.extend(relativeTime)
export default function () {
  const configProvider: any = inject("configProvider")
  const locale = ref("zh-cn")
  watch(
    configProvider,
    (val) => {
      // console.log()
      locale.value = val?.locale?.locale
    },
    {
      immediate: true,
    }
  )

  return {
    _time(val: string) {
      dayjs.locale(locale.value)
      return dayjs(val).fromNow(true)
    },
  }
}
