import { defineStore } from "pinia"
import { base } from "@/services"
import { Params } from "@/utils/httpTool"
// import { getTagData } from "@/services/base"

const useStore = defineStore("baseStore", {
  state: () => ({
    navList: [],
    wzData: [] as any,
    tabData: [] as any,
    tabChildData: [], //tab子路由 数据
    tagData: [], //标签组件 数据
    gdData: [] as any,
    knowledgeTypeList: [],
    readList:[],
    zsData: [],
    tjData: [], //推荐数据

    knowledgeList:[],
    articleTypeList:[],
    articleTagList:[],
  }),
  actions: {
    async getNavData() {
      const { data } = await base.getNavPage()
      this.$patch({
        navList: data[0].filter((item: any) => item.id),
      })
    },
    // 文章数据
    async getWzFn() {
      //文章请求
      const { data } = await base.getWz() //base的请求方法
      this.$patch({
        wzData: data[0],
      })
      return data
    },
    async getReadList() {
      const { data } = await base.getReadList()
      this.$patch({
        readList: data[0],
      })
      return data
    },
    //tj 数据
    async getTuiJian() {
      //文章tab请求数据
      const { data } = await base.getTuiJian() //base的请求方法
      // console.log(data, "推荐数据")
      this.$patch({
        tjData: data,
      })
    },
    async getArticleTagList() {
      const { data } = await base.getTage()
      this.$patch({
        articleTagList: data,
      })
      return data
    },
    //
    async getTabData() {
      const { data } = await base.gettabData()
      // console.log(data, "tab数据") //tab 每一项数据
      this.$patch({
        tabData: data,
      })
    },
    // 动态子路由 接口
    async getTabChild(type: string, params: Params) {
      const { data } = await base.gettabChild(type, params)
      console.log(data[0],"123???");
      
      this.$patch({
        tabChildData: data, // 文章 下tab 每一项数据
      })
    },
    async getArticleTypeList() {
      const { data } = await base.getTypeList()
      this.$patch({
        articleTypeList: data,
      })
      return data
    },
    //归档
    async getgd() {
      //文章请求
      const { data } = await base.getgd() //base的请求方法
      // console.log(data, "归档数据")
      this.$patch({
        gdData: data,
      })
    },
    async getNavKnowledge() {
      const { data } = await base.getNavKnowledge();
      console.log("data1", data[0]);
      
      this.$patch({
        knowledgeList: data[0],
      });
    },
    async getKnowledgeTypeList() {
      const { data } = await base.getTypeList();
      this.$patch({
        knowledgeTypeList: data,
      });
      return data;
    },
    //注册小册
    async getZc() {
      //文章请求
      const { data } = await base.getZcData() //base的请求方法
      // console.log(data[0], "注册小册")
      this.$patch({
        zsData: data[0],
      })
    },
    async getTagData() {
      const { data } = await base.getTagData() //base的请求方法
      // console.log(data, "标签数据")
      this.$patch({
        tagData: data,
      })
    },
  },
})

// console.log(useStore)
export default useStore
