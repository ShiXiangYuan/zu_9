import {
  Button,
  message,
  Layout,
  Carousel,
  Tabs,
  ConfigProvider,
  Pagination,
  Card,
  Tag,
} from "ant-design-vue"

import { App } from "vue"

const install = (app: App): void => {
  app.use(Button)

  app.use(Layout)
  app.use(Carousel)
  app.use(Card)
  app.use(Tag)

  app.use(ConfigProvider)
  app.use(Pagination)

  app.config.globalProperties.$message = message
}

export default {
  install,
}

// use({install(app){}})
