import BaseHeader from "@/components/baseHeader/index.vue"
import { App } from "vue"
import icon from "@/components/iconFont"
import Tuijian from "@/components/rigBox/tuijian.vue"
import tagEle from "@/components/rigBox/TagElement.vue"
import BaseList from "../components/baseList/index.vue";
// 注册公共组件
export default {
  install(app: App): void {
    app.component("base-header", BaseHeader)
    app.component("base-list", BaseList)
    app.component("tj", Tuijian)
    app.component("tagvue", tagEle)
    Object.keys(icon).forEach((key) => {
      app.component(key, icon[key])
    })
  },
}
