const baseRouter = [
  {
    path: "/article",
    component: () => import("views/article/index.vue"),
    children: [
      {
        path: "/article/category/:id",
        component: () => import("views/category/index.vue"),
        meta: {
          title: "分类",
        },
      },
    ],
    meta: {
      title: "文章",
      nav: true,
    },
  },
  {
    path: "/archives",
    component: () => import("views/archives/index.vue"),
    meta: {
      title: "归档",
      nav: true,
    },
  },
  {
    path: "/knowledge",
    component: () => import("views/knowledge/index.vue"),
    meta: {
      title: "知识小册",
      nav: true,
    },
  },
  {
    path: "/page/:id",
    component: () => import("views/page/index.vue"),
    meta: {
      nav: false,
    },
  },
  {
    path: "/detail/:id",
    component: () => import("views/detail/index.vue"),
    meta: {
      nav: false,
    },
  },
  {
    path: "/",
    redirect: "/article",
  },

  {
    path: "/detail2/:id",
    component: () => import("../views/knowledgeDetail/index.vue"),
    meta: {
      nav: false,
    },
  },

]

export default baseRouter
