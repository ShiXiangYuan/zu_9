import http from "@/utils/httpTool"
import { Params } from "@/utils/httpTool"

export const getNavPage = (params: Params = {}) => http.get("/api/page", params)
// 文章请求部分
export const getWz = (params: Params = {}) => http.get("api/article", params) //全部文章数据

export const getTuiJian = (params: Params = {}) =>
  http.get("/api/article/recommend", params) //推荐阅读

export const gettabData = (params: Params = {}) =>
  http.get("/api/category", params) //获取数据

export const getReadList = (params: Params = {}) =>
  http.get("/api/article/recommend", params)
  
export const getTage = () =>
  http.get("/api/tag", {
    articleStatus: "publish",
  })
export const getNavKnowledge = (params: Params = {}) =>
  http.get("/api/knowledge", params)

export const getTypeList = (params: Params = {}) =>
  http.get("/api/category", {
    articleStatus: "publish",
  })



export const gettabChild = (type: string, params: Params) =>
  http.get("/api/article/category" + "/" + type, { ...params }) //获取 tab内容数据 getDetailData

export const getDetailData = (id:string) =>
  http.get("/api/article/"+id,{id}) //详情

export const getTagData = (params: Params = {}) => http.get("/api/tag", params)

// 归档请求部分
export const getgd = (params: Params = {}) =>
  http.get("/api/article/recommend", params)

// 注册部分
export const getZcData = (params: Params = {}) =>
  http.get("api/knowledge", params)
